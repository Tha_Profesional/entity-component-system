/*
 * Title: rectangle.h
 *
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef RECTANGLE_H_INCLUDED
#define RECTANGLE_H_INCLUDED

#include "shape.h"
#include "shapename.h"

struct Rectangle : Shape {
    Vector HalfSize;

    Vector Corners[4];

    int Rotation;

    Rectangle() : Shape(ShapeName::Rectangle) {}
};

#endif // RECTANGLE_H_INCLUDED

/*
 * Title: shape.h
 *
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef SHAPE_H_INCLUDED
#define SHAPE_H_INCLUDED

#include "shapename.h"

struct Shape {
    enum ShapeName Name;

    Shape() : Name(ShapeName::None) {}

    Shape(ShapeName sn) : Name(sn) {}
};

#endif // SHAPE_H_INCLUDED


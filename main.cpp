/*
 * Title: main.cpp
 *
 * Special thanks to:
 *      - Bitsquid: http://bitsquid.blogspot.com.au/2014/08/building-data-oriented-entity-system.html
 *                  http://bitsquid.blogspot.com.au/2014/09/building-data-oriented-entity-system.html
 *                  http://bitsquid.blogspot.com.au/2010/09/custom-memory-allocation-in-c.html
 *                  http://www.gamedev.net/page/resources/_/technical/general-programming/c-custom-memory-allocation-r3010
 *
 * Notes:
 * 		- This is a sand-box for an entity component system.
 */

// TODO: Read this: http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3551.pdf

using namespace std;

int main(int argc, char* args[])
{
    return 0;
}

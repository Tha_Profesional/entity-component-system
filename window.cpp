/*
 * Title: window.cpp
 *
 * Project: Project Burger
 *
 * Coded by, and property of John Siciarz (tha.profesional@gmail.com)
 *
 * Version: 0.610
 *
 * Created:	 	14 November 2013
 * Modified:	22 May 2014
 *
 * Special thanks to:
 *      - The SDL wiki page: https://wiki.libsdl.org/
 *      - This post for help with sound file formats:
 *          http://gamedev.stackexchange.com/questions/20672/what-quality-should-my-sounds-be
 *
 * Notes:
 * 		- None
 */

#include "window.h"

#include <cstring>

#include "SDL2/SDL_image.h"
#include "SDL2/SDL_mixer.h"

#include "globals.h"

using namespace std;

/* Name: SDLHelper
 * Description: Constructor.
 * Input:   None
 * Output:  None
 */
Window::Window() : window(nullptr, SDL_DestroyWindow), renderer(nullptr, SDL_DestroyRenderer) {}

/* Name: ~SDLHelper
 * Description: Destructor.
 * Input:   None
 * Output:  None
 *
 */
Window::~Window() {
    Mix_CloseAudio();
    Mix_Quit();

    IMG_Quit();

    SDL_Quit();
}

/* Name: initSDL
 * Description: Start SDL.
 * Input:   None
 * Output:  Type    Description
 *          bool    Returns true if SDL started correctly.
 */
bool Window::initSDL() {
    return (SDL_Init(SDL_INIT_EVERYTHING) == 0);
}

/* Name: initSDLImage
 * Description: Start the SDL Image library.
 * Input:   None
 * Output:  Type    Description
 *          bool    Returns true if SDLImage started correctly.
 */
bool Window::initSDLImage() {
    return ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) == IMG_INIT_PNG);
}

/* Name: initSDLMixer
 * Description: Start the SDL Mixer library.
 * Input:   None
 * Output:  Type    Description
 *          bool    Returns true if SDLMixer started correctly.
 */
bool Window::initSDLMixer() {
    return ((Mix_Init(MIX_INIT_OGG) == MIX_INIT_OGG) && (Mix_OpenAudio(Globals::AUDIO_FREQUENCY, MIX_DEFAULT_FORMAT, Globals::AUDIO_CHANNELS, Globals::AUDIO_CHUNKSIZE) != -1));
}

/* Name: initSDLTTF
 * Description: Start the SDL True Type Font library.
 * Input:   None
 * Output:  Type    Description
 *          bool    Returns true if SDLTTF started correctly.
 */
bool Window::initSDLTTF() {
    return (TTF_Init() != -1);
}

/* Name: createWindow
 * Description: Create the SDL window.
 * Input:   None
 * Output:  Type    Description
 *          bool    Returns true if the window was created properly.
 */
bool Window::createWindow() {
    window.reset(SDL_CreateWindow("", 100, 100, Globals::SCREEN_WIDTH, Globals::SCREEN_HEIGHT, SDL_WINDOW_HIDDEN));

    return window.get() != nullptr;
}

/* Name: createRenderer
 * Description: Create the SDL renderer and displays the window.
 * Input:   None
 * Output:  Type    Description
 *          bool    Returns true if the renderer was created properly.
 */
bool Window::createRenderer() {
    renderer.reset(SDL_CreateRenderer(window.get(), -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC));

    if (renderer.get() != nullptr) {
        SDL_SetRenderDrawColor(renderer.get(), 0, 0, 0, 255);
        SDL_ShowWindow(window.get());
        Render();

        return true;
    }

    return false;
}

/* Name: Init
 * Description: Initialises the SDL components.
 * Input:   None
 * Output:  Type    Description
 *          bool    Returns true if the components were created properly.
 */
bool Window::Init() {
    return initSDL() && initSDLImage() && createWindow() && createRenderer() && initSDLMixer() && initSDLTTF();
}

/* Name: Render
 * Description: Render whatever's been put in the renderer.
 * Input:   None
 * Output:  None
 */
void Window::Render() {
    SDL_RenderPresent(renderer.get());
}

/* Name: ClearRenderer
 * Description: Clear the renderer.
 * Input:   None
 * Output:  None
 */
void Window::ClearRenderer() {
    SDL_RenderClear(renderer.get());
}

/* Name: GetRenderer
 * Description: Returns the games renderer.
 * Input:   None
 * Output:  Type            Description
 *          SDL_Renderer*   A pointer to the games renderer.
 */
SDL_Renderer* Window::GetRenderer() {
    return renderer.get();
}

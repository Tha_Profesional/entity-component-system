/*
 * Title: physicscomponentmanager.cpp
 *
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#include "physicscomponentmanager.h"

using namespace std;

ComponentId PhysicsComponentManager::Create(Entity e) {
    if (map.find(e.Id) != map.end())
        return { -1 };

    collection.Entity.push_back(e);
    collection.InverseMass.push_back(0.0);
    collection.Friction.push_back(0.0);
    collection.Elasticity.push_back(0.0);
    collection.Shape.push_back(Shape());

    map[e.Id] = collection.Entity.size() - 1;

    return { map[e.Id] };
}

void PhysicsComponentManager::Destroy(ComponentId i) {
    int lastIndex = collection.Entity.size() - 1;
    Entity entity = collection.Entity[i.Id];

    collection.Entity[i.Id] = collection.Entity[lastIndex];
    collection.InverseMass[i.Id] = collection.InverseMass[lastIndex];
    collection.Friction[i.Id] = collection.Friction[lastIndex];
    collection.Elasticity[i.Id] = collection.Elasticity[lastIndex];
    collection.Shape[i.Id] = collection.Shape[lastIndex];

    collection.Entity.erase(collection.Entity.begin() + lastIndex);
    collection.InverseMass.erase(collection.InverseMass.begin() + lastIndex);
    collection.Friction.erase(collection.Friction.begin() + lastIndex);
    collection.Elasticity.erase(collection.Elasticity.begin() + lastIndex);
    collection.Shape.erase(collection.Shape.begin() + lastIndex);

    map[collection.Entity[i.Id].Id] = i.Id;
    map.erase(map.find(entity.Id));
}

ComponentId PhysicsComponentManager::GetId(Entity e) {
    if (map.find(e.Id) == map.end())
        return { -1 };
    else
        return { map[e.Id] };
}

float PhysicsComponentManager::GetInverseMass(ComponentId i) {
    return collection.InverseMass[i.Id];
}

void PhysicsComponentManager::SetInverseMass(ComponentId i, float im) {
    collection.InverseMass[i.Id] = im;
}

float PhysicsComponentManager::GetFriction(ComponentId i) {
    return collection.Friction[i.Id];
}

void PhysicsComponentManager::SetFriction(ComponentId i, float f) {
    collection.Friction[i.Id] = f;
}

float PhysicsComponentManager::GetElasticity(ComponentId i) {
    return collection.Elasticity[i.Id];
}

void PhysicsComponentManager::SetElasticity(ComponentId i, float e) {
    collection.Elasticity[i.Id] = e;
}

Shape PhysicsComponentManager::GetShape(ComponentId i) {
    return collection.Shape[i.Id];
}

void PhysicsComponentManager::SetShape(ComponentId i, Shape s) {
    collection.Shape[i.Id] = s;
}

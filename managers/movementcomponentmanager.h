/*
 * Title: movementcomponentmanager.h
 *
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef MOVEMENTCOMPONENTMANAGER_H_INCLUDED
#define MOVEMENTCOMPONENTMANAGER_H_INCLUDED

#include <unordered_map>

#include "movementcomponentcollection.h"
#include "entity.h"
#include "componentid.h"
#include "vector.h"

// TODO: If no other components come in collections - just rename the folder to 'ComponentCollections'
class MovementComponentManager {
private:
    MovementComponentCollection collection;

    std::unordered_map<unsigned, unsigned> map;
public:
    ComponentId Create(Entity e);
    void Destroy(ComponentId i);

    // TODO: Do I need this? It's nice - but not really how I roll.
    //void GarbageCollect(const EntityManager& em);

    ComponentId GetId(Entity e);

    Vector GetVelocity(ComponentId i);
    void SetVelocity(ComponentId i, Vector v);
};

#endif // MOVEMENTCOMPONENTMANAGER_H_INCLUDED

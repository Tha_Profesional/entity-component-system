/*
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef STATICRENDERCOMPONENTMANAGER_H_INCLUDED
#define STATICRENDERCOMPONENTMANAGER_H_INCLUDED

#include <unordered_map>

#include "staticrendercomponentcollection.h"
#include "componentid.h"
#include "textureid.h"

class StaticRenderComponentManager {
private:
    StaticRenderComponentCollection collection;

    std::unordered_map<unsigned, unsigned> map;
public:
    ComponentId Create(Entity e);
    void Destroy(ComponentId i);

    ComponentId GetId(Entity e);

    TextureId GetTextureId(ComponentId i);
    void SetTextureId(ComponentId i, TextureId t);

    SDL_Rect GetClip(ComponentId i);
    void SetClip(ComponentId i, SDL_Rect r);

    Position GetPosition(ComponentId i);
    void SetPosition(ComponentId i, Position p);
};

#endif // STATICRENDERCOMPONENTMANAGER_H_INCLUDED


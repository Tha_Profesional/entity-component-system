/*
 * Title: physicscomponentmanager.h
 *
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef PHYSICSCOMPONENTMANAGER_H_INCLUDED
#define PHYSICSCOMPONENTMANAGER_H_INCLUDED

#include <unordered_map>

#include "physicscomponentcollection.h"
#include "componentid.h"
#include "shape.h"

class PhysicsComponentManager {
private:
    PhysicsComponentCollection collection;

    std::unordered_map<unsigned, unsigned> map;
public:
    ComponentId Create(Entity e);
    void Destroy(ComponentId i);

    ComponentId GetId(Entity e);

    float GetInverseMass(ComponentId i);
    void SetInverseMass(ComponentId i, float im);

    float GetFriction(ComponentId i);
    void SetFriction(ComponentId i, float f);

    float GetElasticity(ComponentId i);
    void SetElasticity(ComponentId i, float e);

    Shape GetShape(ComponentId i);
    void SetShape(ComponentId i, Shape s);
};

#endif // PHYSICSCOMPONENTMANAGER_H_INCLUDED

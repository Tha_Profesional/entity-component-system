/*
 * Title: vector.h
 *
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef VECTOR_H_INCLUDED
#define VECTOR_H_INCLUDED

struct Vector {
    float X;
    float Y;
};

#endif // VECTOR_H_INCLUDED

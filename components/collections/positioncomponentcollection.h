/*
 * Title: positioncomponentcollection.h
 *
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef POSITIONCOMPONENTCOLLECTION_H_INCLUDED
#define POSITIONCOMPONENTCOLLECTION_H_INCLUDED

#include <vector>

#include "entity.h"
#include "Vector.h"

struct Entity;

struct PositionComponentCollection {
    std::vector<struct Entity> Entity;
    std::vector<Vector> Position;
    std::vector<int> Rotation;
};

#endif // POSITIONCOMPONENTCOLLECTION_H_INCLUDED

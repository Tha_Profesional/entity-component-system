/*
 * Title: shapename.h
 *
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef SHAPENAME_H_INCLUDED
#define SHAPENAME_H_INCLUDED

enum class ShapeName {
    None,
    Rectangle,
    Circle
};

#endif // SHAPENAME_H_INCLUDED

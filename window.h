/*
 * Title: window.h
 *
 * Project: Project Burger
 *
 * Coded by, and property of John Siciarz (tha.profesional@gmail.com)
 *
 * Version: 0.810
 *
 * Created:	 	14 November 2013
 * Modified:	22 May 2014
 *
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef WINDOW_H_INCLUDED
#define WINDOW_H_INCLUDED

#include <memory>

#include "SDL2/SDL.h"
#include "SDL2/SDL_ttf.h"

// A wrapper class for some SDL functionality.
class Window { // TODO: Singleton?
private:
    // The SDL window and renderer.
    std::unique_ptr<SDL_Window, void (*)(SDL_Window*)> window;
    std::unique_ptr<SDL_Renderer, void (*)(SDL_Renderer*)> renderer;

    // Initialisation functions.
    bool initSDL();

    bool initSDLImage();
    bool initSDLMixer();
    bool initSDLTTF();

    bool createWindow();
    bool createRenderer();
public:
    // The constructor and destructor.
    Window();
    ~Window();

    // Function to initialise the helper.
    bool Init();

    // Control the render.
    void Render();
    void ClearRenderer();

    // Getter.
    SDL_Renderer* GetRenderer();
};

#endif // WINDOW_H_INCLUDED

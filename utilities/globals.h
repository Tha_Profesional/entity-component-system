/*
 * Title: globals.h
 *
 * Project: entity-component-system
 *
 * Coded by, and property of John Siciarz (tha.profesional@gmail.com)
 *
 * Version: 0.010
 *
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef GLOBALS_H_INCLUDED
#define GLOBALS_H_INCLUDED

namespace Globals {
    const unsigned ENTITY_INDEX_BITS = 24;
    const unsigned ENTITY_INDEX_MASK = (1 << ENTITY_INDEX_BITS) - 1;

    const unsigned ENTITY_GENERATION_BITS = 8;
    const unsigned ENTITY_GENERATION_MASK = (1 << ENTITY_GENERATION_BITS) - 1;

    const unsigned MINIMUM_FREE_INDICES = 1024;

    const unsigned AUDIO_FREQUENCY = 44100;
    const unsigned AUDIO_CHANNELS = 2;
    const unsigned AUDIO_CHUNKSIZE = 2048;

    const unsigned SCREEN_WIDTH = 600;
    const unsigned SCREEN_HEIGHT = 480;
}

#endif // GLOBALS_H_INCLUDED

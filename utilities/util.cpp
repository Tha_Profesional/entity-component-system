/*
 * Title: util.cpp
 *
 * Project: Project Burger
 *
 * Coded by, and property of John Siciarz (tha.profesional@gmail.com)
 *
 * Version: 1.300
 *
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#include "util.h"

#include <sstream>

#include "globals.h"

using namespace std;

namespace Util {

string ToString(int i) {
    /* BUG: This should be changed to to_string().
     * However mingw 32 bit doesn't support it at the moment.
     */
    ostringstream ss;
    ss << i;
    return ss.str();
}

}

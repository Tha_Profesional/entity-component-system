/*
 * Title: util.h
 *
 * Project: Project Burger
 *
 * Coded by, and property of John Siciarz (tha.profesional@gmail.com)
 *
 * Version: 0.100
 *
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef UTIL_H_INCLUDED
#define UTIL_H_INCLUDED

#include <string>

// Random utility functions which are used throughout the game.
namespace Util {
    std::string ToString(int i);
}

#endif // UTIL_H_INCLUDED
